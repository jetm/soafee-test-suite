FROM arm64v8/alpine:3.17.2

RUN apk update && \
    apk add \
        iproute2-minimal=6.0.0-r1 \
        iputils=20211215-r0 \
        perf=5.15.74-r0 \
        stress-ng=0.14.00-r0
