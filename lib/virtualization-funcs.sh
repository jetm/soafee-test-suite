#!/usr/bin/env bash
#
# Copyright (c) 2021-2022, Arm Limited.
# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

is_xendomains_running() {
  systemctl is-active xendomains 2>> "${TEST_STDERR_FILE}"
}

create_guest_vm() {
  local xl_create="sudo -n xl create"

  xl_output=$(${xl_create} "${1}" 2>> "${TEST_STDERR_FILE}")
  local exitcode="${?}"

  if [ "${exitcode}" != 0 ]; then
    echo "${xl_output}"
    return "${exitcode}"
  fi

  return "${exitcode}"
}

destroy_guest_vm() {
  local xl_destroy="sudo -n xl destroy"

  _run get_guest_vm_status "${1}"
  [ "${status}" -ne 0 ] && return 0

  xl_output=$(${xl_destroy} "${1}" 2>> "${TEST_STDERR_FILE}")
  local exitcode="${?}"

  if [ "${exitcode}" != 0 ]; then
    echo "${xl_output}"
    return "${exitcode}"
  fi

  return "${exitcode}"
}

get_guest_vm_status() {
  local xl_list="sudo -n xl list"
  local xl_output

  xl_output=$(${xl_list} "${1}" 2>> "${TEST_STDERR_FILE}")
  local exitcode="${?}"

  if [ "${exitcode}" != 0 ]; then
    echo "${xl_output}"
    return "${exitcode}"
  fi

  local guest_vm_status

  # return the letter status from State column
  guest_vm_status=$(echo "${xl_output}" |
    awk '$5 ~ /-/ {gsub("-", "", $5); print $5}' \
      2>> "${TEST_STDERR_FILE}")

  echo "${guest_vm_status}"
  return "${exitcode}"
}

is_guest_vm_running() {
  local status=0
  local output=""

  _run get_guest_vm_status "${1}"
  if [ "${status}" -eq 2 ]; then
    return 2
  elif [ "${status}" -ne 0 ] && [ "${output}" != "b" ]; then
    return 1
  else
    return 0
  fi
}

is_not_guest_vm_running() {
  # FAIL if there is a problem getting status from guest VM
  # FAIL if guest VM status is different to 'p' -> pause
  # Otherwise PASS

  local guest_vm_status
  guest_vm_status=$(get_guest_vm_status "${1}")

  # shellcheck disable=SC2181
  if [ "${?}" -ne 0 ] && [ "${guest_vm_status}" != "p" ]; then
    return 1
  else
    return 0
  fi
}

is_guest_vm_initialized() {
  _run wait_for_success 300 10 is_guest_vm_running "${1}"
  if [ "${status}" -ne 0 ]; then
    echo "Timeout reached before Guest VM '${1}' is running"
    return "${status}"
  fi

  return 0
}

