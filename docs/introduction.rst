############
Introduction
############

SOAFEE compliance test to determine whether a software stack is SOAFEE
compliant or not.


Feedback and Support
********************

Please, submit an issue at https://gitlab.com/soafee/soafee-test-suite/-/issues.

Maintainer(s)
*************

- Javier Tia <javier.tia@linaro.org>
