..
 # Copyright (c) 2023, Linaro Ltd
 #
 # SPDX-License-Identifier: MIT

#################################
Add tests to SOAFEE Test Suite
#################################

All tests are under `tests` directory divided by features like `Containers` and
`Virtualization`. How to write the tests is important to read `Writing tests
<https://bats-core.readthedocs.io/en/stable/writing-tests.html>`_ from bats
documenation.
