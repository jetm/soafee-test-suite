..
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

##########
User Guide
##########

.. toctree::
   :maxdepth: 1
   :caption: Explain how to run SOAFEE Test Suite

   how_run_tests

Covers the steps necessary to run the tests

.. toctree::
   :maxdepth: 1
   :caption: How to add tests in SOAFEE Test Suite

   how_add_tests

Describes how to add tests in SOAFEE Test Suite.

.. toctree::
   :maxdepth: 1
   :caption: Known Issues

   known_issues

Describes known issues related to the tests behavior.

