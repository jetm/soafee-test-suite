#!/usr/bin/env bash
#
# Copyright (c) 2021-2022, Arm Limited.
# Copyright (c) 2022, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# We need to ensure that we don't run bats from the current user ${HOME} to
# avoid permission denied error, since at some point it will `cd "$OLDPWD"`
# when we already switched to ${SOAFEE_TEST_ACCOUNT} via the following sudo
# command.

set -eu

# Must be at the beginning of script
pushd "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")" >/dev/null && {
  export TEST_DIR="$PWD"
  popd >/dev/null
}

export TEST_SUITE_NAME=${TEST_SUITE_NAME:-user-accounts-integration-tests}

if ! cd /home; then
  echo "FAIL:${TEST_SUITE_NAME}"
  exit 1
fi

# SUDO user with no password and no password required to use sudo
# TODO: check if a sudo user is required. Maybe all can be run under the test
# user
export SUDO_USER=soafee_admin

# All tests will run under this user
export NORMAL_USER=soafee_test

SOAFEE_TEST_ACCOUNT=${NORMAL_USER}
SOAFEE_TEST_BATS_OPTS=${SOAFEE_TEST_BATS_OPTS:---show-output-of-passing-tests}

export TEST_RUNTIME_DIR="${TEST_RUNTIME_DIR:-/var/run/soafee-integration-tests}"
if [[ ! -d "${TEST_RUNTIME_DIR}" ]]; then
  sudo mkdir --parents "${TEST_RUNTIME_DIR}"
  sudo chown "${SOAFEE_TEST_ACCOUNT}" "${TEST_RUNTIME_DIR}"
fi

if sudo -HEnu "${SOAFEE_TEST_ACCOUNT}" bats "${SOAFEE_TEST_BATS_OPTS}" "${TEST_DIR}/${TEST_SUITE_NAME}.bats"; then
  echo "PASS:${TEST_SUITE_NAME}"
  exit 0
else
  echo "FAIL:${TEST_SUITE_NAME}"
  sudo -HEns <<-'EOF'
		if [ -f "${TEST_RUNTIME_DIR}/latest-tests.log" ]; then
			echo "DEBUG:Printing the contents of the log-file:"
			cat "${TEST_RUNTIME_DIR}/latest-tests.log"
		else
			echo "DEBUG:Could not find a log-file."
		fi
		EOF
  exit 1
fi
